FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/register-0.0.1-SNAPSHOT.jar register.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/register.jar"]